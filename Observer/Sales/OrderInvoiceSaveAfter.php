<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\AutomaticallySendInvoice\Observer\Sales;

use \Psr\Log\LoggerInterface;

class OrderInvoiceSaveAfter implements \Magento\Framework\Event\ObserverInterface
{

    public function __construct(
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        LoggerInterface                                       $logger,
        \Kowal\AutomaticallySendInvoice\Helper\Config         $config
    )
    {
        $this->invoiceSender = $invoiceSender;
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {

        if($enable = $this->config->getGeneralCfg('enable', 0)) {
            $invoice = $observer->getEvent()->getInvoice();
            $order = $invoice->getOrder();

            if (!$order->getId()) {
                throw new LocalizedException(__('The order no longer exists.'));
            }

            if (!$invoice->getEmailSent()) {

                try {
                    try {
                        $this->invoiceSender->send($invoice);
                    } catch (\Exception $e) {
                        $this->logger->error($e->getMessage());
                    }

                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        }
    }


}

